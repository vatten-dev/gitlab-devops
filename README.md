# DevOps服務說明
IP：192.168.105.22
帳號：vatten

## TODOs
- [ ] gitlab-pages使用http並固定以pages預設8090port提供靜態頁面網站服務，尚未應用ssl以及安裝wildcard-dns憑證。

- [ ] gitlab-runner使用docker並在同一個vm上執行，實際應用若提供多人多專案執行job，相信需要換成獨立vm單獨跑，或是一台vm起單獨gitlab-runner cluster（docker-swarm）方式處理。

## 網域資訊

DevOps網域
```log
devops.vatten
-> 22
跑single-mode
  -> *.devops.vatten / *.pages.devops.vattion
```

[將ds2備份的gitlab復原至dhub紀錄](./docs/gitlab-backup-recovery.md)

## 常用小工具

CentOS
```bash
# 包含 nslookup dig host
$ yum -y install bind-utils
# 包含 ifconfig netstat
$ yum -y install net-tools
# iostat
$ yum -y install sysstat
```
Debain、Ubuntu
```bash
# 1. vim
apt-get install vim
# or
apt-get install vim-tiny
# ping
apt-get install iputils-ping
# dns-tools(nslookup|dig)
apt-get install dnsutils
# curl
apt-get install curl
```


## 透過minica產出相關ca檔案語法

有關minica操作與知識全部移轉至`gitbook/dockerlearn/devops/self-signed-certificate/minica.md`

### 整合gitlab:runner使用/certs目錄取得ca.crt

gitlab-runner container預設讀取`CA_CERTIFICATES_PATH`環境變數來取得目錄下`ca.crt`檔安裝在container內信任憑證機構位置，因此把minica.pem複製成ca.crt。

參考[Run GitLab Runner in a container](https://docs.gitlab.com/runner/install/docker.html#installing-trusted-ssl-server-certificates)
> The gitlab/gitlab-runner image is configured to look for the trusted SSL certificates at /etc/gitlab-runner/certs/ca.crt, this can however be changed using the -e "CA_CERTIFICATES_PATH=/DIR/CERT" configuration option.
> Copy the `ca.crt` file into the certs directory on the data volume (or container). The `ca.crt` file should contain the **root certificates of all the servers you want GitLab Runner to trust.** The GitLab Runner container will import the ca.crt file on startup so if your container is already running you may need to restart it for the changes to take effect.


## Nginx-proxy-manager
1. nginx-proxy-manager可以解決nginx-proxy在設定上不方便問題
  * 每個compose服務需要額外多加兩筆環境變數。
  * 難以確認是否proxy正常或者目前設定內容。
2. registry、webui-registry、gitlab都有可被信任的CA憑證
  * gitlab可接受自簽憑證
  * gitlab-runner跟gitlab通訊間可使用自簽憑證

## docker-registry
- private docker repos
- 直接抓取官方image `registry:2` 啟動container。
- 以token auth方式由gitlab端提供API進行認證，並直接與gitlab平台帳號做整合管理。

### 整合gitlab:container_registry功能
有關container_registry設定紀錄全部移轉至[此文件](./docs/gitlab-registry-token-auth.md)


## gitlab
- docker-compose.yml內gitlab服務片段。
- 使用已配置好的container image
  - sameersbn/gitlab:13.9.4
- 需要redis與postgresql服務。
- 目前仍使用private-ip搭配指定ssl port(8443)作法來提供https連線。
  - <https://192.168.105.22:8443>
  - 透過dns以及nginx-proxy服務，已可以用<https://gitlab.vatten.docker:8443/>來連線。
- 預設需要開啟http/https/ssh三個對外連線。
  - http|https擇一

### gitlab服務產生的各項log檔
透過以下設定，可將container內寫入的log直接mount在host path以便直接查看。
```yaml
...
volumes:
  - ./gitlab/log:/home/git/gitlab/log:Z
...
```
主要整個執行過程重要的log檔如下
* application.log
  * 啟動gitlab階段訊息
* gitlab-workhorse.log
  * 實際執行gitlab功能URI時紀錄訊息（相當於nginx access.log）
* production.log
  * 整個gitlab服務主要紀錄檔
* sidekiq.log
  * 以json格式呈現gitlab系統狀態資訊檔
* puma.stdout.log
  * gitlab-puma套件執行紀錄檔，若有錯誤會存在 puma.stderr.log

### gitlab-pages功能
有關gitlab-pages紀錄全部移轉至[此文件](./docs/gitlab-pages.md)

### gitlab-runner機制
有關gitlab-runner設定紀錄全部移轉至[此文件](./docs/gitlab-runner.md)

2022-07-13
[升版紀錄](./docs/upgrade-sameersbn-docker-gitlab.md)
[gitlab v14.0.x功能差異](./docs/gitlab-14-newfeature.md)

===
[5月之前實作架設devops測試過程紀錄文件](./docs/devops-log-before-202105.md)


docker run --name gitlab -it --rm \
    sameersbn/gitlab:13.9.4 app:rake gitlab:backup:create
