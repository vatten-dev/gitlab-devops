# gitlab 14.0.x

* all-new responsive views improve the navigation experience on smaller screens.
* In GitLab 14.0 we've redesigned and restructured the left sidebar for improved usability, consistency, and discoverability. We've moved some links to features around, split up features in the Operations menu into three distinct menus, improved visual contrast, and optimized spacing so all the menu items can fit comfortably on a smaller screen.


* GitLab Workflow version 3.21.0 for Visual Studio Code (VS Code) now supports the complete merge request review process, including threads. Select the GitLab icon in VS Code to open the sidebar to display Merge requests I'm reviewing. Select a merge request overview to view the complete details and discussions of the merge request.