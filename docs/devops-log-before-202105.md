# 2021-04-29以前處理紀錄

重新以 minica 工具產出的 private-key 產生 gitlab.vatten.docker 用的 CA憑證

```bash
$openssl genrsa -out gitlab.vatten.docker.key 2048

$openssl req -new -key gitlab.vatten.docker.key -out gitlab.vatten.docker.csr

# interact message...
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:TW
State or Province Name (full name) []:Taipei
Locality Name (eg, city) [Default City]:Taipei
Organization Name (eg, company) [Default Company Ltd]:VattenSoft
Organizational Unit Name (eg, section) []:RD
Common Name (eg, your name or your server\'s hostname) []:gitlab.vatten.docker
Email Address []:sanhuang@vattensoft.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
# interact message...

$openssl x509 -req -days 365 -in gitlab.vatten.docker.csr -CA minica.pem -CAkey minica-key.pem -CAcreateserial -signkey minica-key.pem -out gitlab.vatten.docker.crt

# interact message...
Signature ok
subject=/C=TW/ST=Taipei/L=Taipei/O=VattenSoft/OU=RD/CN=gitlab.vatten.docker/emailAddress=sanhuang@vattensoft.com
Getting Private key
# interact message...
```

```bash
$openssl genrsa -out registry.vatten.docker.key 2048

$openssl req -new -key registry.vatten.docker.key -out registry.vatten.docker.csr

$openssl x509 -req -days 3650 -in registry.vatten.docker.csr -CA minica.pem -CAkey minica-key.pem -CAcreateserial -out registry.vatten.docker.crt
```


若gitlab執行過程發生錯誤，會儲存在`/home/git/gitlab/log/application_json.log`內。

**錯誤狀況**
> container_registry 500錯誤原因：ssl的網域未符合需要重新配置。
```json
{
  "extra.class": "ContainerRegistry::Client",
  "extra.url": "https://registry/v2/gitlab-instance-178dff43/demo/tags/list",
  "exception.class": "Faraday::SSLError",
  "exception.message": "SSL_connect returned=1 errno=0 state=error: certificate verify failed (Hostname mismatch)",
}
```

**錯誤狀況**
> container_registry 500錯誤原因：self-signed certificate ssl不合法。
```json
{
  "extra.class": "ContainerRegistry::Client",
  "extra.url": "https://registry/v2/gitlab-instance-178dff43/demo/tags/list",
  "exception.class": "Faraday::SSLError",
  "exception.message": "SSL_connect returned=1 errno=0 state=error: certificate verify failed (self signed certificate)",
}
```

目前將docker-registry以http提供聯繫先避免掉此一狀況，使用http配置**不需要以下參數**
```yaml
services:
  gitlab:
    enviornment:
      - SSL_REGISTRY_KEY_PATH=/certs/registry.key
      - SSL_REGISTRY_CERT_PATH=/certs/registry.crt
```