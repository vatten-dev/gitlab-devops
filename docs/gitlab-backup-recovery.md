# gitlab備份復原紀錄

## 完成後目前devops使用資訊

**internal-dns 192.168.105.22(預設dns port)**
連線VPN情況下可使用網域替代各個IP連到服務。

**nginx_proxy_manager <http://devops.vatten:81/login>**
以設定internal-dns情況下，透過此服務可管理代轉所有以105.22這個IP為host各項服務(gitlab-devops-platform)。

**private docker registry <https://registry.devops.vatten/v2/>**
內部docker images repository，透過gitlab API實作token authen。

**gitlab https://gitlab.devops.vatten/**
內部devops平台，主要啟用個別專案具備git|webhook|static pages|CD/CI workflow等機制。


## 前置規劃
gitlab服務預設備份檔儲存路徑在 `/gitlab/data/backups`，會依照執行備份處理，完成後產出時間戳記命名的規則檔名，在復原時會判斷此檔案做解壓縮與取代對應路徑等流程。

備份執行最後會提示需手動備份設定檔，實測以`sameersbn/gitlab`執行的gitlab服務**不需要**。
```log
Warning Your `gitlab.rb` and `gitlab-secrets.json` files contain sensitive data and are not included in this backup.
```

需手動複製檔案到復原測試路徑檔案：
  1. `./gitlab/data/backups`，需要將目錄內的備份壓縮檔丟檔新服務對應路徑下再跑restore指令，檔名需照舊。
  2. `./gitlab_runner/config`，runner執行時套用的config.tomi檔，不一定要備份。
  3. `./nginx_proxy_manager/proxy-data`，此為服務實際做reverse-proxy代理機制程式內容，webui內相關檔案則儲存在`/nginx_proxy_manager/mysql`內。


## ds2(192.168.105.81)備份流程
```bash
$ docker-compose run --rm gitlab app:rake
...
gitlab:backup:create
Creating network "dockerhub_net" with driver "bridge"
Creating gitlab_redis      ... done
Creating gitlab_postgresql ... done
Creating dhub_gitlab_run   ... done
Loading /etc/docker-gitlab/runtime/env-defaults
Initializing logdir...
Initializing datadir...
Updating CA certificates...
Installing configuration templates...
Gitlab pages nginx proxy disabled
Assuming custom domain setup with own HTTP(S) load balancer'
Configuring gitlab...
Configuring gitlab::database.
Configuring gitlab::redis
Configuring gitlab::secrets...
Configuring gitlab::sidekiq...
Configuring gitlab::gitaly...
Configuring gitlab::monitoring...
Configuring gitlab::gitlab-workhorse...
Configuring gitlab::puma...
Configuring gitlab::timezone...
Configuring gitlab::rack_attack...
Configuring gitlab::ci...
Configuring gitlab::artifacts...
Configuring gitlab::lfs...
Configuring gitlab::uploads...
Configuring gitlab::mattermost...
Configuring gitlab::project_features...
Configuring gitlab::oauth...
Configuring gitlab::ldap...
Configuring gitlab::cron_jobs...
Configuring gitlab::backups...
Configuring gitlab::registry...
Configuring gitlab::pages...
Configuring gitlab::sentry...
Configuring gitlab-shell...
Configuring gitlab-pages...
Configuring nginx...
Configuring nginx::gitlab...
Configuring nginx::gitlab::ssl...
Configuring nginx::gitlab::hsts...
Configuring nginx::gitlab-registry...
Configuring nginx::gitlab-pages...
Gitlab pages nginx proxy disabled
Assuming custom domain setup with own HTTP(S) load balancer'
gitlab_extensions:cron: stopped
gitlab_extensions:nginx: stopped
gitlab_extensions:sshd: stopped
gitlab:puma: stopped
gitlab:gitlab-workhorse: stopped
Running raketask gitlab:backup:create...
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:29: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:30: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
/usr/lib/ruby/vendor_ruby/rubygems/defaults/operating_system.rb:10: warning: constant Gem::ConfigMap is deprecated
2021-05-17 20:38:04 +0800 -- Dumping database ...
Dumping PostgreSQL database gitlabhq_production ... [DONE]
2021-05-17 20:38:09 +0800 -- done
2021-05-17 20:38:09 +0800 -- Dumping repositories ...
 * gitlab-instance-178dff43/Monitoring (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b) ...
 * gitlab-instance-178dff43/Monitoring (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b) ... [SKIPPED]
 * gitlab-instance-178dff43/Monitoring.wiki (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki) ...
 * gitlab-instance-178dff43/Monitoring.wiki (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.wiki) ... [SKIPPED]
 * gitlab-instance-178dff43/Monitoring.design (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.design) ...
 * gitlab-instance-178dff43/Monitoring.design (@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.design) ... [SKIPPED]
 * gitlab-instance-178dff43/demo (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35) ...
 * gitlab-instance-178dff43/demo (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35) ... [SKIPPED]
 * gitlab-instance-178dff43/demo.wiki (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki) ...
 * gitlab-instance-178dff43/demo.wiki (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.wiki) ... [SKIPPED]
 * gitlab-instance-178dff43/demo.design (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.design) ...
 * gitlab-instance-178dff43/demo.design (@hashed/d4/73/d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.design) ... [SKIPPED]
 * dockerize-apps/dockerizehub (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce) ...
 * dockerize-apps/dockerizehub (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce) ... [SKIPPED]
 * dockerize-apps/dockerizehub.wiki (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki) ...
 * dockerize-apps/dockerizehub.wiki (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.wiki) ... [SKIPPED]
 * dockerize-apps/dockerizehub.design (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.design) ...
 * dockerize-apps/dockerizehub.design (@hashed/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.design) ... [SKIPPED]
 * dockerize-apps/devicecontrol (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d) ...
 * dockerize-apps/devicecontrol (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d) ... [SKIPPED]
 * dockerize-apps/devicecontrol.wiki (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki) ...
 * dockerize-apps/devicecontrol.wiki (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.wiki) ... [SKIPPED]
 * dockerize-apps/devicecontrol.design (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.design) ...
 * dockerize-apps/devicecontrol.design (@hashed/ef/2d/ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d.design) ... [SKIPPED]
 * dockerize-apps/bootstrap (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683) ...
 * dockerize-apps/bootstrap (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683) ... [SKIPPED]
 * dockerize-apps/bootstrap.wiki (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki) ...
 * dockerize-apps/bootstrap.wiki (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.wiki) ... [SKIPPED]
 * dockerize-apps/bootstrap.design (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.design) ...
 * dockerize-apps/bootstrap.design (@hashed/e7/f6/e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683.design) ... [SKIPPED]
 * dockerize-apps/webrestful (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451) ...
 * dockerize-apps/webrestful (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451) ... [SKIPPED]
 * dockerize-apps/webrestful.wiki (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki) ...
 * dockerize-apps/webrestful.wiki (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.wiki) ... [SKIPPED]
 * dockerize-apps/webrestful.design (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.design) ...
 * dockerize-apps/webrestful.design (@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451.design) ... [SKIPPED]
 * dockerize-apps/vattenlib (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3) ...
 * dockerize-apps/vattenlib (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3) ... [SKIPPED]
 * dockerize-apps/vattenlib.wiki (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki) ...
 * dockerize-apps/vattenlib.wiki (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.wiki) ... [SKIPPED]
 * dockerize-apps/vattenlib.design (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.design) ...
 * dockerize-apps/vattenlib.design (@hashed/2c/62/2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3.design) ... [SKIPPED]
 * sanhuang/dockage (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5) ...
 * sanhuang/dockage (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5) ... [DONE]
 * sanhuang/dockage.wiki (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki) ...
 * sanhuang/dockage.wiki (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.wiki) ... [SKIPPED]
 * sanhuang/dockage.design (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.design) ...
 * sanhuang/dockage.design (@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.design) ... [SKIPPED]
 * sanhuang/authentication (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8) ...
 * sanhuang/authentication (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8) ... [DONE]
 * sanhuang/authentication.wiki (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki) ...
 * sanhuang/authentication.wiki (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.wiki) ... [SKIPPED]
 * sanhuang/authentication.design (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.design) ...
 * sanhuang/authentication.design (@hashed/4f/c8/4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8.design) ... [SKIPPED]
 * sanhuang/pagesdemo (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918) ...
 * sanhuang/pagesdemo (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918) ... [DONE]
 * sanhuang/pagesdemo.wiki (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918.wiki) ...
 * sanhuang/pagesdemo.wiki (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918.wiki) ... [SKIPPED]
 * sanhuang/pagesdemo.design (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918.design) ...
 * sanhuang/pagesdemo.design (@hashed/6b/51/6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918.design) ... [SKIPPED]
 * sanhuang/prtelearning-book (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278) ...
 * sanhuang/prtelearning-book (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278) ... [DONE]
 * sanhuang/prtelearning-book.wiki (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278.wiki) ...
 * sanhuang/prtelearning-book.wiki (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278.wiki) ... [SKIPPED]
 * sanhuang/prtelearning-book.design (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278.design) ...
 * sanhuang/prtelearning-book.design (@hashed/3f/db/3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278.design) ... [SKIPPED]
2021-05-17 20:38:11 +0800 -- done
2021-05-17 20:38:11 +0800 -- Dumping uploads ...
2021-05-17 20:38:11 +0800 -- done
2021-05-17 20:38:11 +0800 -- Dumping builds ...
2021-05-17 20:38:11 +0800 -- done
2021-05-17 20:38:11 +0800 -- Dumping artifacts ...
2021-05-17 20:38:13 +0800 -- done
2021-05-17 20:38:13 +0800 -- Dumping pages ...
2021-05-17 20:38:16 +0800 -- done
2021-05-17 20:38:16 +0800 -- Dumping lfs objects ...
2021-05-17 20:38:16 +0800 -- done
2021-05-17 20:38:16 +0800 -- Dumping container registry images ...
2021-05-17 20:38:16 +0800 -- done
Creating backup archive: 1621255096_2021_05_17_13.9.4_gitlab_backup.tar ... done
Uploading backup archive to remote storage  ... skipped
Deleting tmp directories ... done
done
done
done
done
done
done
done
done
Deleting old backups ... skipping
Warning: Your gitlab.rb and gitlab-secrets.json files contain sensitive data
and are not included in this backup. You will need these files to restore a backup.
Please back them up manually.
Backup task is done.
```

### dns與nginx_proxy_manager服務移轉紀錄

dns本身只要維護`docker_dns/mappings.json`這個檔案即可，由於轉移後理論上只需在devops(192.168.105.22)上提供internal-dns-server，因此必須調整加入適用於`devops.vatten`相關映對配置。

nginx_proxy_manager由於透過node來進行reverse-proxy轉換處理，測試過無法直接將ds2(192.168.105.81)上的目錄檔案移轉後直接啟動服務。
> 事實上也沒必要，因為reverse-proxy必須跑在主要docer services上統一接收80與443請求進行轉換，所以也是得在devops.vatten上重建。
但針對`proxy_mysql`的`npm`資料庫可以直接匯入來自ds2原本的內容，因此我先在ds2將proxy_mysql的3306對外匯出npm資料庫後，在devops.vatten上啟動proxy_mysql(暫停proxy_manager狀態)清空資料庫再匯入處理，測試成功可登入計有帳號與相關設定，只是設定部分無法使用！

**補充** 關於代理網域使用的ca憑證在proxy_manager上會把內容取出儲存於db內的`certificate` Table的`meta` 欄位。


## devops(192.168.105.22) 復原紀錄

### 前置動作
1. ssh登入devops
2. 移除22上原本多餘檔案目錄，僅保留以下兩個目錄
    1. ./centos7_packages/
    2. ./images/docker/registry/v2/
3. 過程中internal gitlab無法使用，暫時先轉移到gitlab.com上。pull git到DevOps目錄
```bash
$git clone https://gitlab.com/vatten-dev/gitlab-devops.git DevOps
```
4. 先起dns以及proxy_manager服務並完成配置新網域對應修改
```bash
$docker-compose up -d dns proxy_manager
```

### 復原dns/proxy_manager指令重點紀錄
scp複製整個nginx_proxy_manager目錄，由於docker-compose.yml內bind-mount volume使用`:Z`參數掛載，主要再跑service內使用IO預設會以`root`身份處理，因此部分檔案將無法直接以一般帳號進行複製。
```bash
$sudo scp -r /home/vatten/dhub/nginx_proxy_manager mysql vatten@192.168.105.22:/home/vatten/DevOps
```

### 加裝minica服務處理產出devops憑證

下載docker minica
```bash
# add container minica in docker
$docker pull ryantk/minica

# minica usage 使用當前使用者執行minica
$ docker run --user $(id -u):$(id -g) -it -v /home/vatte/DevOps/certs:/output ryantk/minica --domains dockserv.vatten

# 以root帳號執行minica，產出的ca檔若以一般帳號會看不到檔案（但有目錄），必須用hostOS上的root chown之後才看得到。
$docker run --user root -it -v /home/vatte/DevOps//certs:/output ryantk/minica --domains *.devops.vatten
```

**透過minica.pem產生gitlab/registry網域使用的crt/key憑證檔**

產出用來加密domain憑證用金鑰
```bash
$openssl genrsa -out registry.vatten.docker.key
```

直接提供csr設定值略過互動輸入來產出向rootCA申請憑證請求檔
```bash
$openssl req -new -key registry.vatten.docker.key -subj "/C=TW/ST=Taipei/O=VattenSoft/CN=registry.vatten.docker" -out registry.vatten.docker.csr
```

以minica.pem作為rootCA產出domain憑證
```bash
$openssl x509 -req -days 3560 -in registry.vatten.docker.csr -CA minica.pem -CAkey minica-key.pem -CAcreateserial -out registry.vatten.docker.crt
```


產生token auth cert/key給 gitlab以API登入registry使用
```bash
$openssl req -new -newkey rsa:4096 -nodes \
 -x509 -days 3650 \
 -subj "/CN=gitlab-issuer" \
 -keyout auth/registry.key \
 -out authtoken/registry.cert
```



## 復原gitlab紀錄

新IP使用docker-compose指令重新跑docker-compose，但先起db部分
```bash
$cd DevOps/

$docker-compose up -d gitlab_redis gitlab_postgresql
```

gitlab服務初始化約需要將近2分鐘以上，必須先起postgresql、redis，若使用links配置服務關聯，記得networks設定要在同一個network(name)之下。

在`DevOps/gitlab/log/production.log`內有看到以下片段才可能正常開啟gitlab頁面
```log
...
Started GET "/" for 172.16.0.1 at 2021-05-19 16:16:43 +0800
Processing by RootController#index as HTML
Redirected to https://gitlab.devops.vatten/users/sign_in
Filter chain halted as :redirect_unlogged_user rendered or redirected
...
```

將ds2上備份檔scp到devops
```bash
$scp gitlab/data/backups/1621255096_2021_05_17_13.9.4_gitlab_backup.tar vatten@192.168.105.22:/home/vatten/DevOps/gitlab/data/backups/
```

關閉/移除gitlab後直接執行restore程序
```bash
$docker run --name gitlab -it --rm \
    -e DB_ADAPTER=postgresql \
    -e DB_HOST=gitlab_postgresql \
    -e DB_PORT=5432 \
    -e DB_USER=gitlab \
    -e DB_PASS=gitlab123 \
    -e DB_NAME=gitlabhq_production \
    -e REDIS_HOST=gitlab_redis \
    -e REDIS_PORT=6379 \
    -e GITLAB_SECRETS_DB_KEY_BASE=Rvsdfmqlk3jejo2ijtio13flkdnflknri1hf3mflmmfesf \
    -e  GITLAB_SECRETS_SECRET_KEY_BASE=vmsdkljl2j3flkvmk2hgnvk2lnfgo24irjfFewffjh13f \
    -e  GITLAB_SECRETS_OTP_KEY_BASE=cnmznckqhfcnkeakdkldbbWCWWdwad31uyi3y \
    --network=dockerhub_net \
    sameersbn/gitlab:13.9.4 app:rake db:setup

$docker run --name gitlab -it --rm \
    -e DB_ADAPTER=postgresql \
    -e DB_HOST=gitlab_postgresql \
    -e DB_PORT=5432 \
    -e DB_USER=gitlab \
    -e DB_PASS=gitlab123 \
    -e DB_NAME=gitlabhq_production \
    -e REDIS_HOST=gitlab_redis \
    -e REDIS_PORT=6379 \
    -e GITLAB_SECRETS_DB_KEY_BASE=Rvsdfmqlk3jejo2ijtio13flkdnflknri1hf3mflmmfesf \
    -e  GITLAB_SECRETS_SECRET_KEY_BASE=vmsdkljl2j3flkvmk2hgnvk2lnfgo24irjfFewffjh13f \
    -e  GITLAB_SECRETS_OTP_KEY_BASE=cnmznckqhfcnkeakdkldbbWCWWdwad31uyi3y \
    --network=dockerhub_net \
    -v /home/vatten/DevOps/gitlab/data:/home/git/data:Z \
    sameersbn/gitlab:13.9.4 app:rake  gitlab:backup:restore
```

**進行db::setup發生錯誤訊息**
```bash
...
Missing Rails.application.secrets.openid_connect_signing_key for production environment. The secret will be generated and stored in config/secrets.yml.
Database 'gitlabhq_production' already exists
psql:/home/git/gitlab/db/structure.sql:1: ERROR:  schema "gitlab_partitions_dynamic" already exists
rake aborted!
failed to execute:
psql -v ON_ERROR_STOP=1 -q -X -f /home/git/gitlab/db/structure.sql --single-transaction gitlabhq_production

Please check the output above for any errors and make sure that `psql` is installed in your PATH and has proper permissions.
...
```

因此嘗試先把原本執行中的gitlab_postgresql刪除重建。
```bash
# 停止移除gitlab_postgresql
$docker-compose stop gitlab_postgresql ; docker-compose rm -f gitlab_postgresql

# 刪除 gitlab_postgresql 使用的 volumes(專案目錄在DevOps下)
$docker volume rm devops_postgresql-data
```

### 再次嘗試利用openssl以minica.pem來產出重建registry與gitlab使用的ca憑證檔

#### registry.devops.vatten部分
```bash
# 產出私鑰檔
$openssl genrsa -out registry.devops.vatten.key
# 產出csr檔
$openssl req -new -key registry.devops.vatten.key -subj "/C=TW/ST=Taipei/O=VattenSoft/CN=registry.devops.vatten" -out registry.devops.vatten.csr
# 以csr與私鑰產出crt憑證檔
$openssl x509 -req -days 3560 -in registry.devops.vatten.csr -CA minica.pem -CAkey minica-key.pem -CAcreateserial -out registry.devops.vatten.crt
```

#### gitlab.devops.vatten部分
```bash
# 產出私鑰檔
$openssl genrsa -out gitlab.devops.vatten.key
# 產出csr檔
$openssl req -new -key gitlab.devops.vatten.key -subj "/C=TW/ST=Taipei/O=VattenSoft/CN=gitlab.devops.vatten" -out gitlab.devops.vatten.csr
# 以csr與私鑰產出crt憑證檔
$openssl x509 -req -days 3560 -in gitlab.devops.vatten.csr -CA minica.pem -CAkey minica-key.pem -CAcreateserial -out gitlab.devops.vatten.crt
```

### 修改docker-compose.yml內相關網域、CA憑證配置部分
參數`SSL_CA_CERTIFICATES_PATH=/certs/minica.pem`是本次主要用來產出網域用的rootCA，設定後在gitlab內會以此加入信任的根憑證，如此才能讓container_registry功能正常呼叫registry端https協定API。

```yaml
    # 調整services: gitlab部分
    ...
volumes:
    - ./auth:/auth:ro
    - ./certs:/certs:ro
    - ./gitlab/data:/home/git/data:Z
    - ./gitlab/log:/home/git/gitlab/log:Z
environment:
    - DEBUG=false
    ...
    # 保持不變
    ...
    - GITLAB_HOST=gitlab.devops.vatten
    ...
    # 保持不變
    ...
    - SSL_CA_CERTIFICATES_PATH=/certs/minica.pem
    - SSL_CERTIFICATE_PATH=/certs/gitlab.devops.vatten/cert.pem
    - SSL_KEY_PATH=/certs/gitlab.devops.vatten/key.pem
    ...
    # 保持不變
    ...
    - GITLAB_REGISTRY_HOST=registry.devops.vatten
    - GITLAB_REGISTRY_API_URL=https://registry.devops.vatten/v2/
    - GITLAB_REGISTRY_KEY_PATH=/auth/registry.key
    - GITLAB_REGISTRY_ISSUER=gitlab-issuer
    - SSL_REGISTRY_CERT_PATH=/certs/registry.devops.vatten/cert.pem
    - SSL_REGISTRY_KEY_PATH=/certs/registry.devops.vatten/key.pem
    ...
    # 保持不變，pages部分暫時關閉配置
    ...
```


## 狀況紀錄

#### SSL錯誤1. certificate verify failed (unable to get local issuer certificate)
```log
"exception.message": "SSL_connect returned=1 errno=0 state=error: certificate verify failed (unable to get local issuer certificate)",
```
> 原因是 環境變數 `SSL_CA_CERTIFICATES_PATH=/certs/minica.pem`未設定。此一參數會將minica.pem（我用來產self-signed-ca的rootCA）加入到container內`/etc/ssl/certs/ca-certificates.crt`內，此為linux上信任的rootCA整個清單，簡易確認是否解決可以bash到container內後以下列指令測試！
> ```bash
> $ curl https://registry.vatten.docker/v2/
> ```
> 若能正常顯示內容代表以套用rootCA



#### SSL錯誤2. certificate verify failed
```log
"exception.message": "SSL_connect SYSCALL returned=5 errno=0 state=error: certificate verify failed",
```
> 最終原因 問題出在 minica.pem檔 並非移轉後重新配置使用的minica.pem檔（放成vatten.docker使用版本！），於是換回devops.vatten使用的版本即可成功解決本錯誤。


##### 推測原因1.與測試紀錄
> 用來做registry auth token對應錯誤，推測應該是產生的指令參數錯誤使用不符合要求的加解密演算法。
> 根據之前處理語法重新產生一次，

```bash
$ openssl req \
    -newkey rsa:4096 \
    -nodes \
    -keyout auth/issuer-auth.key \
    -subj "/CN=gitlab-issuer" \
    -out auth/issuer-auth.csr

Generating a 4096 bit RSA private key
................................++
................................................................................................................................................................................................................................................................................++
writing new private key to 'auth/issuer-auth.key'
-----

$openssl x509 -req \
    -days 3650 \
    -in auth/issuer-auth.csr \
    -signkey auth/issuer-auth.key \
    -out auth/issuer-auth.crt
Signature ok
subject=/CN=gitlab-issuer
Getting Private key
# 完成後，於auth目錄下產生3個檔案
# 1. issuer-auth.key
# 2. issuer-auth.csr
# 3. issuer-auth.crt
```

##### 推測原因2.與測試紀錄
實際上目前devops啟動的gitlab根本沒有正確安裝registry的憑證，或者不明原因導致

1. 嘗試改用原本的auth-token對應檔
  - 將原本vatten.docker整個 /certs替換回去，再把新網域的crt/key檔放回去

2. 執行 `docker exec -it gitlab bash` 在container內先直接測試 `curl https://registry.devops.vatten/v2/`
  - 正常應該直接可以回應registry的body資訊，但會出現
```log
curl: (60) SSL certificate problem: self signed certificate
More details here: https://curl.haxx.se/docs/sslcerts.html
```

3. 於是嘗試手動進入container安裝ca憑證在執行curl仍有問題，於是懷疑minica.pem是否有問題！
- [Generating trusted SSL keys for development · GitHub](https://gist.github.com/mwidmann/115c2a7059dcce300b61f625d887e5dc)

4. 直接使用curl --cacert指向minica.pem檔 但發現仍無法連線取得內容



### 在gitlab container內加裝工具除錯
gitlab預設使用Ubuntu（Debain）OS將以`apt-get`工具管理套件，
預設來說直接可對外（dns使用8.8.8.8），因此先做`apt-get update`套件清單後可正常安裝相關套件。
其中，我常裝的兩個工具是
1. vim
```bash
apt-get install vim
# or
apt-get install vim-tiny
```
2. ping、nslookup
```bash
# ping
apt-get install iputils-ping
# dns-tools(nslookup|dig)
apt-get install dnsutils
```
3. curl
```bash
# curl
apt-get install curl
```

補充：使用curl簡易確認gitlab服務是否正常
curl 'https://gitlab.devops.vatten/-/liveness'


## 線上資源
- [Troubleshooting SSL | GitLab](https://docs.gitlab.com/ee/administration/troubleshooting/ssl.html)
- [Adding trusted root certificates to the server](https://manuals.gfi.com/en/kerio/connect/content/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html)
- [day09_docker07_GitLab02_下集 - iT 邦幫忙::一起幫忙解決難題，拯救 IT 人的一天](https://ithelp.ithome.com.tw/articles/10204317)