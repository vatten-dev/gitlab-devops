# GitLab與docker registry整合應用

介紹文章
* [GitLab Container Registry](https://about.gitlab.com/blog/2016/05/23/gitlab-container-registry/)
* [docker-gitlab/container_registry.md](https://github.com/sameersbn/docker-gitlab/blob/master/docs/container_registry.md)
* [How to setup omnibus installation to use an external Docker registry?](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/25967)

GitLab needs a certificate ("auth token") to talk to the registry API. The tokens must be provided in the /certs directory of your container. You could use an existing domain ceritificate or create your own with a very long lifetime like this:

原本，相關設定是為了在gitlab平台內專案可以使用container_registry功能，後來發現設定啟用後，連同docker-registry的帳號管理一併由gitlab端處理了。

* 開啟認證來登入服務時一次只能指定一種認證方式，因此若是使用htpasswd方式提供由web直接登入帳密作法，就無法在啟用token方式讓gitlab與registry連結使用！


## TODOs
- [ ] 架設docker-registry-v2使用的auth service，可做ACL控制，推測近一步可做為授權服務
  * https://github.com/cesanta/docker_auth
  * 介紹docker-registry與registry-auth-service運作關係
    * https://github.com/distribution/distribution/blob/main/docs/spec/auth/token.md
- [ ] 如何從瀏覽器、postman進行registry認證測試？
  * 目前直接開 https://registry.vatten.docker/v2/ 會直接出現
  `{"errors":[{"code":"UNAUTHORIZED","message":"authentication required","detail":null}]}`
  * 如何把private access token設定在postman內進行請求



## 產出token auth的cert/key檔案語法
```bash
$mkdir certs
$cd certs
# Generate a random password password_file used in the next commands
$openssl rand -hex -out password_file 32
# Create a PKCS#10 certificate request
$openssl req -new -passout file:password_file -newkey rsa:4096 -batch > registry.csr
# Convert RSA key
$openssl rsa -passin file:password_file -in privkey.pem -out registry.key
# Generate certificate
$openssl x509 -in registry.csr -out registry.crt -req -signkey registry.key -days 10000
```

* [docker-gitlab/container_registry.md at master · sameersbn/docker-gitlab · GitHub](https://github.com/sameersbn/docker-gitlab/blob/master/docs/container_registry.md#create-auth-tokens)


## 在compose.yml配置registry使用token auth相關變數
```yaml
registry:
  image: registry:2
  container_name: registry
  ports:
    - "2443:443"
  environment:
    - REGISTRY_STORAGE_DELETE_ENABLED=true
    - REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY=/registry
    - REGISTRY_LOG_LEVEL=info
    - REGISTRY_HTTP_ADDR=0.0.0.0:443
    # 必須指定為可透過docker network連結到gitlab，結尾必須/jwt/auth
    - REGISTRY_AUTH_TOKEN_REALM=https://gitlab:443/jwt/auth
    # 固定參數不變動
    - REGISTRY_AUTH_TOKEN_SERVICE=container_registry
    - REGISTRY_AUTH_TOKEN_ISSUER=gitlab-issuer
    # 需指定來自gitlab配置的CA憑證(self-signed版本)
    - REGISTRY_AUTH_TOKEN_ROOTCERTBUNDLE=/certs/registry.crt
```

## 在compose.yml配置gitlab使用token auth相關變數
```yaml
gitlab:
    image: sameersbn/gitlab:13.9.4
    container_name: gitlab
    volumes:
      # 將憑證檔路徑掛載到container內
      - ./certs:/certs:Z
    environment:
      # Registry
      - GITLAB_REGISTRY_ENABLED=true
      - GITLAB_REGISTRY_HOST=registry.vatten.docker
      # 必須指定為可透過docker network連結到registry，尾碼需加"/"
      - GITLAB_REGISTRY_API_URL=http://registry:443/v2/
      # 必須指定向docker-registry端做jwt登入使用的key！
      - GITLAB_REGISTRY_KEY_PATH=/certs/registry-auth.key
      # 需對應docker-registry設定
      - GITLAB_REGISTRY_ISSUER=gitlab-issuer
      # 不使用ssl的話不需指定
      - GITLAB_REGISTRY_PORT=443
      - SSL_REGISTRY_KEY_PATH=/certs/registry.vatten.docker.key
      - SSL_REGISTRY_CERT_PATH=/certs/registry.vatten.docker.crt
```


## 2021-05-13應用測試

驗證從client端(OSX cli)可以登入 private docker registry作法
```bash
# 測試1. 失敗：憑證對應錯誤，應該是registry產出的registry.crt內包含的CN(Common Name)與網域不一致。
san@mini ~ % docker login registry.vatten.docker
Username: sanhuang
Password:
Error response from daemon: Get https://registry.vatten.docker/v2/: x509: certificate is not valid for any names, but wanted to match registry.vatten.docker

# 測試2. 失敗：同上，與是否使用完整FQDN無關
san@mini ~ % docker login https://registry.vatten.docker/
Username: sanhuang
Password:
Error response from daemon: Get https://registry.vatten.docker/v2/: x509: certificate is not valid for any names, but wanted to match registry.vatten.docker

# 測試3. 失敗：已更換重建了憑證，錯誤顯示登入資訊錯誤，使用gitlab建立的帳號
san@mini ~ % docker login https://registry.vatten.docker/
Username: sanhuang
Password:
Error response from daemon: Get https://registry.vatten.docker/v2/: x509: certificate signed by unknown authority

# 經確認前述錯誤是因為使用介面登入用的password，實際上必須從gitlab -> account -> edit profile -> access token取得一組可以存取api的 Priacate Accesss Token當作密碼來登入！
# 測試4. 失敗：Bad Gateway, 由於在compose.yml內設定container互聯使用registry與gitlab並未加上domainname(vatten.docker)，推測導致此一問題。
san@mini ~ % docker login -u sanhuang -p xp7ZvhCz1hcZZPgjQ1ZP https://registry.vatten.docker/
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Error response from daemon: Get https://registry.vatten.docker/v2/: Get https://gitlab/jwt/auth?account=sanhuang&client_id=docker&offline_token=true&service=container_registry: Bad Gateway


# 終於成功！同時 https://gitlab.vatten.docker/sanhuang/dockage/container_registry 也開啟正常！
san@mini ~ % docker login -u sanhuang -p xp7ZvhCz1hcZZPgjQ1ZP https://registry.vatten.docker/
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Login Succeeded
san@mini ~ %
```