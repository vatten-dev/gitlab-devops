
## 狀況1. 從13.9.4版本升級至14.0.6之後發生問題
> 主要狀況是直接進入project issues頁面會出錯，但進入board檢視卻又正常

之前升級流程是備份後嘗試從13.9.4升到14.0.6然後再直接升到14.3.0結果頁面無法開啟！於是再切換回14.0.6版本使用可正常登入。
使用中發現會在issues頁面出現500錯誤，今天進入gitlab container內檢視`application.log`找出以下關鍵錯誤訊息。
> ActionView::Template::Error (PG::UndefinedTable: ERROR:  relation "services" does not exist

```log
Started GET "/vattenproject/tsmc_develop/-/issues" for 172.16.0.1 at 2022-08-02 12:20:09 +0800
Processing by Projects::IssuesController#index as HTML
  Parameters: {"namespace_id"=>"vattenproject", "project_id"=>"tsmc_develop"}
  Rendered layout layouts/project.html.haml (Duration: 3.3ms | Allocations: 997)
Completed 500 Internal Server Error in 199ms (ActiveRecord: 86.2ms | Elasticsearch: 0.0ms | Allocations: 49547)

ActionView::Template::Error (PG::UndefinedTable: ERROR:  relation "services" does not exist
LINE 8:  WHERE a.attrelid = '"services"'::regclass
                            ^
):
     9:   = auto_discovery_link_tag(:atom, safe_params.merge(rss_url_options).to_h, title: "#{@project.name} issues")
    10:
    11: .js-jira-issues-import-status{ data: { can_edit: can?(current_user, :admin_project, @project).to_s,
    12:   is_jira_configured: @project.jira_service.present?.to_s,
    13:   issues_path: project_issues_path(@project),
    14:   project_path: @project.full_path } }
    15:

lib/gitlab/database/schema_cache_with_renamed_table.rb:25:in `columns'
lib/gitlab/database/schema_cache_with_renamed_table.rb:29:in `columns_hash'
app/views/projects/issues/index.html.haml:12
app/controllers/application_controller.rb:128:in `render'
app/controllers/application_controller.rb:487:in `set_current_admin'
lib/gitlab/session.rb:11:in `with_session'
app/controllers/application_controller.rb:478:in `set_session_storage'
lib/gitlab/i18n.rb:99:in `with_locale'
lib/gitlab/i18n.rb:105:in `with_user_locale'
app/controllers/application_controller.rb:472:in `set_locale'
app/controllers/application_controller.rb:466:in `set_current_context'
lib/gitlab/middleware/speedscope.rb:13:in `call'
lib/gitlab/request_profiler/middleware.rb:17:in `call'
lib/gitlab/jira/middleware.rb:19:in `call'
lib/gitlab/middleware/go.rb:20:in `call'
lib/gitlab/etag_caching/middleware.rb:21:in `call'
lib/gitlab/middleware/multipart.rb:172:in `call'
lib/gitlab/middleware/read_only/controller.rb:50:in `call'
lib/gitlab/middleware/read_only.rb:18:in `call'
lib/gitlab/middleware/same_site_cookies.rb:27:in `call'
lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'
lib/gitlab/middleware/basic_health_check.rb:25:in `call'
lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'
lib/gitlab/middleware/request_context.rb:21:in `call'
config/initializers/fix_local_cache_middleware.rb:11:in `call'
lib/gitlab/middleware/rack_multipart_tempfile_factory.rb:19:in `call'
lib/gitlab/metrics/requests_rack_middleware.rb:74:in `call'
lib/gitlab/middleware/release_env.rb:12:in `call'
```

參考此篇[ERROR: relation "services" does not exist after upgrade to 14.2.0 (#6352) · Issues · GitLab.org / omnibus-gitlab · GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6352)內文在後面有提到可以**升級至14.1.1版本**，解決此問題！

過程中順便學習如何指令操作我用`sameersbn/gitlab`架設的gitlab

檢測gitlab環境狀態指令
```bash
# 進入gitlab
$docker exec -it gitlab bash
# 預設身份為root，先切換為git帳號
root@f193374a15fa:/home/git/gitlab# su - git
# 切換路徑到./gitlab
git@f193374a15fa:~$ cd gitlab

# 執行檢測gitlab環境狀態指令
git@f193374a15fa:~/gitlab$ bundle exec rake gitlab:check RAILS_ENV=production
Checking GitLab subtasks ...

Checking GitLab Shell ...

GitLab Shell: ... GitLab Shell version >= 13.19.0 ? ... OK (13.19.0)
Running /home/git/gitlab-shell/bin/check
Internal API available: OK
Redis available via internal API: OK
gitlab-shell self-check successful

Checking GitLab Shell ... Finished

Checking Gitaly ...

Gitaly: ... default ... OK

Checking Gitaly ... Finished

Checking Sidekiq ...

Sidekiq: ... Running? ... yes
Number of Sidekiq processes (cluster/worker) ... 0/1

Checking Sidekiq ... Finished

Checking Incoming Email ...

Incoming Email: ... Reply by email is disabled in config/gitlab.yml

Checking Incoming Email ... Finished

Checking LDAP ...

LDAP: ... LDAP is disabled in config/gitlab.yml

Checking LDAP ... Finished

Checking GitLab App ...

Git configured correctly? ... yes
Database config exists? ... yes
All migrations up? ... yes
Database contains orphaned GroupMembers? ... no
GitLab config exists? ... yes
GitLab config up to date? ... yes
Log directory writable? ... yes
Tmp directory writable? ... yes
Uploads directory exists? ... yes
Uploads directory has correct permissions? ... yes
Uploads directory tmp has correct permissions? ... yes
Init script exists? ... yes
Init script up-to-date? ... yes
Projects have namespace: ...
GitLab Instance / Monitoring ... yes
Dockerize Apps / devicecontrol ... yes
Dockerize Apps / bootstrap ... yes
Dockerize Apps / webrestful ... yes
Dockerize Apps / vattenlib ... yes
Dockerize / Vatten Python ... yes
Dockerize Apps / authentication ... yes
SanHuang / DNSTools ... yes
Csharp / TraSideSlope ... yes
ENC / ENC Web React ... yes
ENC / ENC Client ... yes
ENC / ENC Server ... yes
ENC / ENC Lib ... yes
SanHuang / Typer ... yes
SanHuang / dockerlearn ... yes
SanHuang / Vatten worklog book ... yes
ENC / Online ENC Documents ... yes
Dockerize / Enc V2 ... yes
Dockerize / Vatten Platform ... yes
Dockerize / Vatten Storage ... yes
Dockerize / Vatten Log ... yes
SanHuang / jekyll ... yes
Dockerize / Gitlab Devops ... yes
Csharp / TraSideSlopeDockerize ... yes
VattenProject / itrisqlserver ... yes
GitLab Instance / Workflow ... yes
VattenProject / twmpwd ... yes
VattenProject / template ... yes
SanHuang / The Algorithms - Python ... yes
VattenProject / eva_air ... yes
VattenProject / tsmc_develop ... yes
VattenProject / vatten_internal ... yes
ENC / LogParser ... yes
VattenProject / kbro_cgnat ... yes
Redis version >= 5.0.0? ... yes
Ruby version >= 2.7.2 ? ... yes (2.7.2)
Git version >= 2.31.0 ? ... yes (2.32.0)
Git user has default SSH configuration? ... yes
Active users: ... 7
Is authorized keys file accessible? ... yes
GitLab configured to store new projects in hashed storage? ... yes
All projects are in hashed storage? ... yes

Checking GitLab App ... Finished


Checking GitLab subtasks ... Finished
```



## 線上資源
* [Maintenance Rake tasks | GitLab](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html#display-status-of-database-migrations) gitlab檢核維護指令
* [Upgrading GitLab | GitLab](https://docs.gitlab.com/ee/update/index.html#upgrade-paths) 升級版本路徑，從特定版升級必須先經過某些版本。

## 狀況2. root帳號登入會直接出現500 Internal Error
經查application.log紀錄
```log
Started POST "/users/sign_in" for 172.16.0.1 at 2022-08-02 13:00:17 +0800

ArgumentError (marshal data too short):

lib/gitlab/middleware/read_only/controller.rb:50:in `call'
lib/gitlab/middleware/read_only.rb:18:in `call'
lib/gitlab/middleware/same_site_cookies.rb:27:in `call'
lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'
lib/gitlab/middleware/basic_health_check.rb:25:in `call'
lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'
lib/gitlab/middleware/request_context.rb:21:in `call'
config/initializers/fix_local_cache_middleware.rb:11:in `call'
lib/gitlab/middleware/rack_multipart_tempfile_factory.rb:19:in `call'
lib/gitlab/metrics/requests_rack_middleware.rb:74:in `call'
lib/gitlab/middleware/release_env.rb:12:in `call'
```

參考[HTTP 500 on Sign In with GitLab 14.0.1 (#334681) · Issues · GitLab.org / GitLab · GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/334681)說明，主要在舊版本(~v14.1.1)之前ruby config檔`config/initializers/rack_attack.rb`有關。因此根據此篇[[Sign In/Sign Out - v14.0.0] Problem with Ruby on Sign In and Sign out · Issue #2373 · sameersbn/docker-gitlab · GitHub](https://github.com/sameersbn/docker-gitlab/issues/2373#issuecomment-879184630)作法在docker-compose.yml內配置環境變數
```yaml
environment:
      - ...
      - RACK_ATTACK_ENABLED=false
```
可(暫時)解決！

### 線上資源
* [HTTP 500 on Sign In with GitLab 14.0.1 (#334681) · Issues · GitLab.org / GitLab · GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/334681)
    * [[Sign In/Sign Out - v14.0.0] Problem with Ruby on Sign In and Sign out · Issue #2373 · sameersbn/docker-gitlab · GitHub](https://github.com/sameersbn/docker-gitlab/issues/2373)

## 狀況3. 忘記gitlab root密碼作法，重設密碼
```bash
# 進入gitlab
$docker exec -it gitlab bash
# 預設身份為root，先切換為git帳號
root@f193374a15fa:/home/git/gitlab# su - git
# 切換路徑到./gitlab
git@f193374a15fa:~$ cd gitlab
# 參考舊版線上文件紀錄使用以下指令會出錯
git@f193374a15fa:~/gitlab$ bundle exec rails c production
# ...
/home/git/gitlab/vendor/bundle/ruby/2.7.0/gems/thor-1.1.0/lib/thor/base.rb:525:in `handle_argument_error`: ERROR: "rails console" was called with arguments ["production"] (Thor::InvocationError)
Usage: "rails console [options]"

# 改用以下指令正常
git@f193374a15fa:~/gitlab$ bundle exec rails console -e production
--------------------------------------------------------------------------------
 Ruby:         ruby 2.7.2p137 (2020-10-01 revision 5445e04352) [x86_64-linux-gnu]
 GitLab:       14.1.1 (c8edb9de30c) FOSS
 GitLab Shell: 13.19.0
 PostgreSQL:   12.3
--------------------------------------------------------------------------------
Loading production environment (Rails 6.1.3.2)

irb(main):001:0> user = User.where(id: 1).first
=> #<User id:1 @root>

# 將root密碼改成root1234
irb(main):002:0> user.password = 'root1234'
=> "root1234"
# root密碼再確認
irb(main):003:0> user.password_confirmation = 'root1234'
=> "root1234"
# 儲存更新
irb(main):004:0> user.save
Enqueued ActionMailer::MailDeliveryJob (Job ID: 5716f0df-0809-465d-b6c0-26ca50f13bd0) to Sidekiq(mailers) with arguments: "DeviseMailer", "password_change", "deliver_now", {:args=>[#<GlobalID:0x0000562722868888 @uri=#<URI::GID gid://gitlab/User/1>>]}
=> true
# 離開
irb(main):005:0> exit
```
> 完成前述動作後，即可於網頁重新使用設定密碼登入！

### 線上資源
* [Reset Root Password · Issue #929 · sameersbn/docker-gitlab · GitHub](https://github.com/sameersbn/docker-gitlab/issues/929)