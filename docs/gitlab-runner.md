# gitlab-runner機制

## docker-compose.yml內配置gitlab_runner服務語法
```yaml
gitlab-runner:
  image: 'gitlab/gitlab-runner:latest'
  container_name: gitlab-runner
  depends_on:
    - 'gitlab'
#   restart: always
  environment:
    - TZ=Asia/Taipei
    - CA_CERTIFICATES_PATH=/certs/gitlab.crt
  volumes:
    - ./gitlab-runner/config:/etc/gitlab-runner:Z
    - ./certs:/certs:Z
    # 當使用docker來跑runner時建議配置local-host上的docker-engine
    - /var/run/docker.sock:/var/run/docker.sock
  networks:
    dockerhub_net:
        aliases:
          - runner.vatten.docker
```

## gitlab應用runner作法

設置後，透過docker指令直接註冊gitlab repo專案使用此runner
```bash
$docker exec -it gitlab_runner bash -c "gitlab-runner register"
Runtime platform                                    arch=amd64 os=linux pid=21 revision=7f7a4bb0 version=13.11.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
# https://gitlab.vatten.docker/
Enter the registration token:
# mjNGtAqoVUfXAxswwqkw
Enter a description for the runner:
# [31d4ecb448c7]: 31d4ecb448c7
Enter tags for the runner (comma-separated):
#
Registering runner... succeeded                     runner=mjNGtAqo
Enter an executor: docker-ssh, parallels, shell, ssh, virtualbox, docker+machine, docker-ssh+machine, kubernetes, custom, docker:
# docker
Enter the default Docker image (for example, ruby:2.6):
# centos:7
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

從docker logs與gitlab網頁上runner區塊確認gitlab-runner已連動可執行jobs
```bash
$docker-compose logs gitlab_runner

gitlab_runner        |
gitlab_runner        | Configuration loaded                                builds=0
gitlab_runner        | listen_address not defined, metrics & debug endpoints disabled  builds=0
gitlab_runner        | [session_server].listen_address not defined, session endpoints disabled  builds=0
gitlab_runner        | Configuration loaded                                builds=0
gitlab_runner        | Configuration loaded                                builds=0
gitlab_runner        | Checking for jobs... received                       job=26 repo_url=https://gitlab.vatten.docker:8443/sanhuang/dockage.git runner=HVqmjnPx
# 當repo上有jobs要執行時會出現類似以下紀錄
gitlab_runner        | WARNING: Job failed: exit code 1                    duration_s=60.731150321 job=26 project=10 runner=HVqmjnPx
gitlab_runner        | WARNING: Failed to process runner                   builds=0 error=exit code 1 executor=docker runner=HVqmjnPx
```

直接在gitlab-runner container裡面產證書
```bash
$openssl s_client -connect 172.16.0.9:443 -showcerts </dev/null 2>/dev/null | sed -e '/-----BEGIN/,/-----END/!d' | tee "172.16.0.9.crt" >/dev/null

$gitlab-runner register --tls-ca-file="172.16.0.9.crt"
```

### gitlab_runner設定檔
container內路徑`/etc/gitlab-runner/config.tomi`，將此上層目錄與./gitlab_runner/conf做繫結。
```bash
...
[[runners]]
  name = "cbcdf02bc749"
  url = "https://gitlab.vatten.docker/"
  token = "iQ3yzozHLy5Drjxe4wn8"
  executor = "docker"
  # 內網連到docker-gitlab必須直接以gitlab container使用443port來連線！
  clone_url = "https://gitlab.vatten.docker:443/"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    # 從外部下載 node image跑container
    image = "node:10"
    # 由於要存取gitlab-runner所在docker volumes配置，與共用docker.sock必須開啟privileged參數
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    # 使用docker模式時，必須配置與gitlab/gitlab-runner同內網才能通訊取得git repo或者對docker-registry進行登入註冊！
    network_mode = "dockerhub_net"
...
```