# gitlab-pages功能

單純啟用很簡單只要在 yaml檔內開啟ENV參數即可
```yaml
  ...
  # Pages
  - GITLAB_PAGES_ENABLED=true
  - GITLAB_PAGES_DOMAIN=gitlabpage.vatten.docker
  - GITLAB_PAGES_PORT=8443
  - GITLAB_PAGES_HTTPS=true
  - GITLAB_PAGES_ACCESS_CONTROL_SERVER=https://gitlab.vatten.docker:8443
  - GITLAB_PAGES_ACCESS_CONTROL=false
  ...
```
但如何使用wirecard dns自動產生pages網域以及URI部分尚未確認，透過.gitlab-ci.yml機制來跑node後轉成範本靜態網站。

## 2021-05-13紀錄
已成功解決連線問題可正常顯示，兩個關鍵，直接使用port8090對外以及關閉gitlab-container的nginx proxy設定以Proxymanager做導向。

```yaml
  - GITLAB_PAGES_ENABLED=true
  - GITLAB_PAGES_DOMAIN=pages.vatten.docker
  - GITLAB_PAGES_PORT=8090
  - GITLAB_PAGES_HTTPS=false
  - GITLAB_PAGES_NGINX_PROXY=false
```

## 2021-05-12紀錄
```yaml
  # Pages
  - GITLAB_PAGES_ENABLED=true
  - GITLAB_PAGES_DOMAIN=pages.vatten.docker
  - GITLAB_PAGES_PORT=8090
  - GITLAB_PAGES_HTTPS=false
  # - GITLAB_PAGES_EXTERNAL_HTTP=:8080
  - GITLAB_PAGES_ARTIFACTS_SERVER=false
  - GITLAB_PAGES_ACCESS_CONTROL=false
  # - GITLAB_PAGES_ACCESS_CONTROL_SERVER=https://gitlab.vatten.docker/
  # - GITLAB_PAGES_ACCESS_REDIRECT_URI=http://projects.page.vatten.docker/auth
  # - GITLAB_PAGES_ACCESS_CLIENT_ID=a9d1ae45ad10f08d45757c9db787dadee0bccb9a070295db0e147b8b87af8e0e
  # - GITLAB_PAGES_ACCESS_CLIENT_SECRET=cff86908a1aa8443f894b61a47d5338d1bfe68608c4afafbb82a0cfc235807df
  # - GITLAB_PAGES_ACCESS_SECRET=<SECRET_KEY>
  # - GITLAB_PAGES_VERSION=1.3.1
  - GITLAB_PAGES_NGINX_PROXY=true
```

目前遇到的問題應該是外部連結nginx-proxy再轉到gitlab內的nginx-proxy導向pages網域會有對應問題
cd - [ ] 是否`GITLAB_PAGES_PORT`需要設定？預設pages使用8090是否需要對外？
- [x] 如何確認gitlab上的pages配置內容以及gitlab上的nginx實際指定pages部分萬用子網域對應規則？
```bash
# gitlab-pages預設專案使用路徑
/home/git/data/shared/pages/[sanhuang/pagesdemo]
```

[目前狀態] 404錯誤，但顯示的nginx server為openresty，應該是有導向gitlab container內的nginx服務。

[nginx - GitLab Pages running in Docker inaccessible from outside the container - Server Fault](https://serverfault.com/questions/1047390/gitlab-pages-running-in-docker-inaccessible-from-outside-the-container)
[Documentation for GITLAB_PAGES_EXTERNAL_HTTP and GITLAB_PAGES_EXTERNAL_HTTPS · Issue #1145 · sameersbn/docker-gitlab · GitHub](https://github.com/sameersbn/docker-gitlab/issues/1145)
