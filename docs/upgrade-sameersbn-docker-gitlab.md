# 嘗試升級devops環境使用的gitlab

## 升級路徑工具
從以下網頁工具可以得知官方建議從a版本號要升級至b版本號，必須先經過哪些版本逐步升級處理。

* [Gitlab 升級路徑 – IT's on Fire](https://coolfire.fetag.org/2020/03/22/gitlab-%E5%8D%87%E7%B4%9A%E8%B7%AF%E5%BE%91/)
    * [gitlab ce Upgrade Path](https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/)

## 備份操作
先將gitlab container關閉下線（postgresql與redis不作動！）
```bash
$docker stop gitlab ; docker rm gitlab
```

直接以下指令進行備份，檔案最後會留存於 ./gitlab/data/backups/xxx.tar.gz
```bash
$docker-compose run --rm gitlab app:rake gitlab:backup:create
```

若完成備份，則直接操作

## 升到14.0.6
2022-07-11 先從13.9.4升到13.12.1
原本使用image: sameersbn/gitlab:13.9.4
過程先升級至13.12.x(13最後子版號) **sameersbn/gitlab:13.12.1**
目前已升至14.0.6版使用中，觀察過程中是否有功能問題，順便確認13->14差異。

## 線上資源
* [GitHub - sameersbn/docker-gitlab: Dockerized GitLab](https://github.com/sameersbn/docker-gitlab#upgrading)
* [sameersbn/gitlab迁移gitlab-ce升级_无限进步～的博客-CSDN博客_sameersbn/gitlab](https://blog.csdn.net/m0_60696455/article/details/120023083)