      Name                     Command                       State                                              Ports
--------------------------------------------------------------------------------------------------------------------------------------------------------
dns                 python server.py                 Up                      0.0.0.0:53->10053/tcp, 0.0.0.0:53->10053/udp
gitlab              /sbin/entrypoint.sh app:start    Up (health: starting)   0.0.0.0:8022->22/tcp, 0.0.0.0:8443->443/tcp, 80/tcp, 0.0.0.0:8090->8090/tcp
gitlab_postgresql   /sbin/entrypoint.sh              Up                      5432/tcp
gitlab_redis        docker-entrypoint.sh --log ...   Up                      6379/tcp
proxy_manager       /init                            Up (healthy)            0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:81->81/tcp
proxy_mysql         /scripts/run.sh                  Up                      0.0.0.0:13306->3306/tcp

